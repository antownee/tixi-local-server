'use strict'
const mongoose = require('mongoose');
const util = require('util');
const bodyParser = require('body-parser');
const express = require("express");
const passport = require('passport');
const User = require('../../models/user');
const async = require('async');
const crypto = require('crypto')


//Used to handle 
//1. Login get and post -- DONE
//2. Signup get and post -- DONE
//3. Forgot password get and post -- DONE
//4. Reset password post and get
//4. Logout -- DONE

//Define before various routers
const router = express.Router();

router.get('/', function (req,res,next) {
    res.redirect('/login')
})

router.get('/login', function (req,res,next) {
    if(req.user){
        return res.redirect('/admin')
    }
    res.render('account/login.pug');
})

router.post('/login', (req,res,next) => {
    req.assert('email', 'Email is not valid').isEmail();
    req.assert('password', 'Password cannot be empty').notEmpty();
    req.sanitize('email').normalizeEmail({remove_dots: false});

    const errors = req.validationErrors();

    if(errors){
        req.flash('errors', errors);
        return res.redirect('/login');
    }

    passport.authenticate('local', (err, user, info) => {
        if(err){ return next(err); }
        if(!user || !(user.odin) ){
            req.flash('errors', info);
            return res.redirect('/login');
        }
        req.logIn(user,(err) => {
            if(err){ return next(err); }
            req.flash('success', {msg: `You've been logged in`});
            return res.redirect('/admin')
        })
    })(req,res,next)
})



router.get('/logout', (req,res,next) => {
    req.logOut();
    req.session.destroy(function(err) {
        // cannot access session here
        if(err){ return next(err)}
        res.redirect('/login')
    })
})


router.get('/forgot', (req,res,next) => {
    //Route to forgot page
    if(req.isAuthenticated()){
        return res.redirect('/admin');
    }
    res.render('account/forgot.pug');
})


router.get('/reset/:token', (req,res, next) => {
    if(req.isAuthenticated()){
        return res.redirect('/admin')
    }

    User.findOne({ passwordResetToken: req.params.token})
    .where('passwordResetExpires').gt(Date.now())
    .exec((err, user) => {
        if(err){ return next(err)};
        if(!user){
            req.flash('errors', {msg: "The password reset token is invalid or has expired"});
            return res.redirect('/forgot');
        }
        res.render('account/reset.pug');
    })
})


module.exports = router;

