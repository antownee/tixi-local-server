'use strict';
const mosca = require('mosca');
require('dotenv').config();

///MODELS
const scan = require("../models/scan");
const cashLoad = require("../models/cashLoad");
const purchase = require("../models/purchase");

const ascoltatore = {
    //using ascoltatore
    type: 'mongo',
    url: process.env.MONGODB_LOCAL,
    pubsubCollection: 'ascoltatori',
    mongo: {}
};

const settings = {
    port: 1883,
    backend: ascoltatore
};

const server = new mosca.Server(settings);

server.on('clientConnected', function (client) {
    console.log('client connected', client.id);
});

server.on('published', (packet, client) => {
    var topic = packet.topic;
    switch (topic) {
        case 'scan':
            //UID
            console.log(packet.topic + ' : ' + packet.payload);
            saveScan(JSON.parse(packet.payload));
            //Save to DB
            break;
        case 'purchase':
            //PURCHASE
            console.log(packet.topic + ' : ' + packet.payload);
            savePurchase(JSON.parse(packet.payload));
            //Save to DB
            break;
        case 'cashload':
            //CASH LOAD
            console.log(packet.topic + ' : ' + packet.payload);
            saveCashLoad(JSON.parse(packet.payload));
            //Save to DB
            break;
        default:
            console.log('Payload: ' + packet.payload);
            break;
    }
});

server.on('ready', () => {
    console.log('Mosca server is up and running');
});


function saveScan(s) {
    var scn = new scan({
        userID: s.UID,
        readerID: s.readerID,
        scannedAt: Date.now()
    });

    scn.save(function (err, data) {
        if (err) { throw err; }
        console.log(data._id + " has been saved at " + data.scannedAt);
    });
}

function savePurchase(p) {
    var purch = new purchase({
        purchaseID: p.purchaseID,
        userID: p.UserID,
        readerID: p.ReaderID,
        merchantID: p.MerchantID,
        itemsPurchased: p.ItemsBought, //Array
        amount: p.Amount,
        preBalance: p.prebalance,
        postBalance: p.postbalance,
        purchasedAt: Date.now() //change this to obtain date from client
    });

    purch.save(function (err) {
        if (err) { throw err; }
        console.log(purch._id + " has been saved");
    })

}


function saveCashLoad(cl) {
    var c = new cashLoad({
        userID: req.body.UID,
        boothID: req.body.BID,
        transactionType: req.body.tType,
        transactionReference: req.body.tRef,
        amount: req.body.amnt,
        balance: req.body.bal,
        loadedAt: Date.now(),
        updatedAt: Date.now(),
        success: req.body.scs
    });

    c.save(function (err, c) {
        if (err) { throw err; }

        console.log(c.userID + " has been loaded with KES " + c.amount + ". TYPE: " + c.transactionType);
    })

}