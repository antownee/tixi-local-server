'use strict'
var util = require('util');
var express = require("express");
const _ = require("lodash");
const async = require('async')

//Define before the users and purchases routers
var router = express.Router();


//HOME, INDEX
router.get("/", function (req, res) {
  res.render("rfid/dash.pug",{
    //Probably pull these from a query from the purchases and merchants collection
      notifications: ["2 White Caps were bought at Mama Johny's",
       "1 hotdog was bought at Mama Rocks",
       "10 shots of Jameson have been bought at 1824",
       "2 sheeshas were bought at Sweet Scents",
       "6 burgers were bought at Mama Rock",
       "3 Carlsbergs were bought at Mwenda's",
       "4 packets of fries were bought at Flame Flavours",
       "10 shots of Jameson have been bought at 1824",
       "2 Tuskers were bought at Mwenda's",
       "1 Tusker was bought at 1824"],
      one : true,
      two: false,
      three: false,
      four: false,
      five: false,
      six: false
    });
})

//Booths
var boothRouter = require("./booths");
router.use("/booths",boothRouter);

//Portals
var portalRouter = require("./portals");
router.use("/portals",portalRouter);

//Merchants
var merchantRouter = require("./merchants");
router.use("/merchants", merchantRouter);

module.exports = router;





