var mongoose = require('mongoose');
var util = require('util');
var booth = require("../../models/booth");
var portal = require("../../models/portal");
var scan = require("../../models/scan");
var express = require("express");
var _ = require("lodash");


var router = express.Router();
module.exports = router;

////Portals
router.get("/", function (req, res) {
  //First, get all the portals in the event. So that means reader model
  //Build the portal object and then search for its scans
  //Add to the new portal model then render view
  portal.find({}, function (err, data) {
    if (err) next(err);
    if (data.length == 0) {
      res.render("rfid/empty.pug", { one: false, two: true, three: false, four: false, five: false, six: false,
        message: "No portals have been registered." });
      return;
    }
    res.render("rfid/portals.pug", { portals: data, one: false, two: true, three: false, four: false, five: false, six: false });
    return;
  })
})


//Scans per portal
router.get("/:id", function (req, res, next) {
  portal.find({ portalID: req.params.id } ,function (err,p) {
      if(err) next(err);
      if(p.length == 0)
        return;
      var prt = p[0];

      scan.find({ readerID: req.params.id }, function (err, s) {
        if (err) next(err);
        if (s.length == 0) {
          res.render("rfid/empty.pug", { one: false, two: true, three: false, four: false, five: false, six: false,
            message:"Portal " + req.params.id  + " has no scan ins at the moment." 
          });
          return;
        }
        //Render the scan data in the view
        res.render("rfid/portalscans.pug", { scans: s, one: false, two: true, three: false, four: false, five: false, six: false,
          portalObject: prt
        });
    });
  })
})