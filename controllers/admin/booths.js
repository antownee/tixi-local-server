'use strict'
var mongoose = require('mongoose');
var util = require('util');
var booth = require("../../models/booth");
var cashLoad = require("../../models/cashLoad");
var express = require("express");
var _ = require("lodash");


var router = express.Router();
module.exports = router;

////Cash booths
router.get("/", function (req, res) {
  booth.find({},function (err,bths) {
    if(err) next(err);
    if(bths.length == 0){
      res.render("rfid/empty.pug", { one: false, two: true, three: false, four: false, five: false, six: false,       
        message: "No booths have been registered." });
        return;
    }
      res.render("rfid/cashBooths.pug", { booths: bths, one: false,two: false,three: true,four: false,five: false, six: false});
  })
})

//Cashload per booth
router.get("/:id", function (req, res) {
  booth.find({ boothID: req.params.id } ,function (err,b) {
      if(err) next(err);
      if(b.length == 0)
        return;
      var bth = b[0];

      cashLoad.find({boothID: req.params.id},function (err,cl) {
      if(err) next(err);
      if(cl.length == 0){
        res.render("rfid/empty.pug", { one: false, two: true, three: false, four: false, five: false,six: false,     
          message: "No cash loads so far have been done." });
          return;
      }

      //Sort the cashloads in form of MPESA and card and total amount
      var mpesaLoads = _.filter(cl,{'transactionType' : "MPESA"});
      var mpesaTotal = _.sumBy(mpesaLoads, 'amount');
      var cardLoads = _.filter(cl,{'transactionType' : "CARD"});
      var cardTotal = _.sumBy(cardLoads, 'amount');
      var cashLoads = _.filter(cl,{'transactionType' : "CASH"});
      var cashTotal = _.sumBy(cashLoads, 'amount');
      
      res.render("rfid/boothcashloads.pug", { 
        boothObject: bth, 
        MPESA: mpesaTotal, 
        CASH: cashTotal, 
        CARD: cardTotal, 
        MPESAtrans: mpesaLoads, 
        CASHtrans: cashLoads, 
        CARDtrans: cardLoads,
        cashLoads: cl, 
        one: false,two: false,three: true,four: false,five: false, six: false
        });
        return;
    });
  });
})