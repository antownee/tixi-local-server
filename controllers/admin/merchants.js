var mongoose = require('mongoose');
var util = require('util');
var merchant = require("../../models/merchant")
var purchase = require("../../models/purchase")
var express = require("express");
var _ = require("lodash");


var router = express.Router();
module.exports = router;

//Merchants
router.get("/", function (req, res) {
  merchant.find({}, function (err, data) {
    if (err) next(err);
    if (data.length == 0) {
      res.render("rfid/empty.pug", { one: false, two: false, three: false, four: true, five: false, six: false,
        message: "No merchants have been registered so far." 
      });
      return;
    }
    res.render("rfid/merchants.pug", { merchants: data, one: false, two: false, three: false, four: true, five: false, six: false });
  })
})

//Merchant summary and detailed information
router.get("/:id", function (req, res) {
  merchant.find({ merchantID:req.params.id } ,function (err,m) {
      if(err) next(err);
      if(m.length == 0)
        return;
      var merch = m[0];

      purchase.find({ merchantID: req.params.id }, function (err, p) {
      if (err) next(err);
      if (p.length == 0) {
        res.render("rfid/empty.pug", { purchases: p, one: false, two: false, three: false, four: true, five: false, six: false,
          message: "No purchases have been made at " + merch.businessName 
        });
        return;
      }
      res.render("rfid/merchantsales.pug", { 
        merchantObject: merch, purchases: p, one: false, two: false, three: false, four: true, five: false, six: false })
      //Render the scan data in the view
    });
  });
})