var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var boothSchema = new Schema({
    boothID: {type: String, unique:true, index:true},
    status: Boolean, //Alive?
    lastcheckedAt: Date
    //Add more information to get from booth.
    //Voltage? Temp? Get as much info as possible
});

var booth = mongoose.model("booth",boothSchema);
module.exports = booth;