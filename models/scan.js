var mongoose = require('mongoose');
var Schema = mongoose.Schema;

//OSR = On site registration. If seen on card, set true
//state = True for in event, False for NOT in event
//success = true for a failed scan attempt
var scanSchema = new Schema({
    userID: String,
    readerID: String,
    eventID: String,
    scannedAt: {type: Date, default: Date.now()},
    state: Boolean,
    OSR: Boolean,
    success: Boolean
});



var scan = mongoose.model("Scan",scanSchema);
module.exports = scan;