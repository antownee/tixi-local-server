'use strict'
const bcrypt = require('bcrypt-nodejs')
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema({
    name: {
        first: String,
        last: String
    },
    email: {type: String},
    password: String,
    passwordResetToken: String,
    passwordResetExpires: Date,
    phoneNumber: String,
    odin: Boolean, 
    createdAt: Date,
    updatedAt: Date
});


//Before you save password, HASH IT!!! THEN save the hash
userSchema.pre("save", function (next) {
    const user = this;
    const cd = new Date();
    this.updatedAt = cd;

    if(!this.createdAt){ 
        this.createdAt = cd;
    }

    if(!user.isModified('password')) { return next();}

    bcrypt.genSalt(10, function (err,salt) {
        bcrypt.hash(user.password, salt, null, function (err,hash) {
            user.password = hash;
            return next();
        })
    })
})

userSchema.methods.comparePassword = function (cp, cb) {
    bcrypt.compare(cp,this.password,(err, isMatch) => {
        cb(err, isMatch);
    });
};

var userSch = mongoose.model("User",userSchema);
module.exports = userSch;