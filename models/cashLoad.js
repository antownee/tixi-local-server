var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var cashLoadSchema = new Schema({
    userID: String,
    boothID: String,
    eventID: String,
    transactionType: String, //MPESA, CARD(debit or credit), CASH also include transaction code for MPESA and card transactions
    transactionReference: { type:String, required: true, unique: true,  index:true},
    amount: {type: Number, required: 'Amount transacted is required'},
    balance: {type: Number, default: 0},
    loadedAt: Date,
    updatedAt: Date,
    success: Boolean
});

var cashLoad = mongoose.model("CashLoad",cashLoadSchema);
module.exports = cashLoad;