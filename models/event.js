const mongoose = require('mongoose');
const autoIncrement = require('mongoose-auto-increment');
const Schema = mongoose.Schema;

const connection = mongoose.createConnection(process.env.MONGODB_LOCAL || process.env.MONGOLAB_URI);
autoIncrement.initialize(connection);

const eventSchema = new Schema({
    eventID: {type: String, unique:true, index:true},
    userID: String,
    eventName: String,
    eventaliasID: Number, //This is the one used in displaying events in the url and all that jazz
    eventLocation: String,
    eventDescription: String,
    startDate: Schema.Types.Mixed,
    endDate: Schema.Types.Mixed,
    posterPath: String
});


eventSchema.plugin(autoIncrement.plugin, {
    model: 'User',
    field: 'eventaliasID',
    startAt: 1001,
    incrementBy: 3
});

const event = mongoose.model("Event",eventSchema);
module.exports = event;