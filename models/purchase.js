var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var purchaseSchema = new Schema({
    purchaseID: {type: String, unique:true, index:true},
    userID: String,
    readerID: String,
    merchantID: String,
    itemsPurchased: [Schema.Types.Mixed],
    amount: {type: Number, required: 'Amount transacted is required'},
    preBalance: Number,
    postBalance: Number,
    purchasedAt: Date
});

//pre function that makes sure that prebalance is greater than postbalance
//it also checks that postbalance + amount = prebalance
//These are just safety checks
var purchase = mongoose.model("Purchase",purchaseSchema);
module.exports = purchase;
