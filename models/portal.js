var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var portalSchema = new Schema({
    portalID: {type: String, unique:true, index:true},
    status: Boolean, //Alive?
    lastcheckedAt: Date
    //Add more information to get from portal.
    //Voltage? Temp? Get as much info as possible
});

var portal = mongoose.model("Portal",portalSchema);
module.exports = portal;