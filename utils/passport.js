'use strict'
const passport = require('passport')
const localStrategy = require('passport-local').Strategy;

//Import user model object
const User = require('../models/user');


passport.serializeUser((user, done) => {
    done(null, user.id);
})

passport.deserializeUser((id, done) => {
    User.findById(id,(err, user) => {
        done(err, user)
    })
})


passport.use(new localStrategy({usernameField: 'email'}, (email, password, done) => {
    User.findOne({email: email.toLowerCase()}, (err, user) => {
        if(err){
            return done(err);
        }
        if(!user){
            return done(null, false, {msg: `Email ${email} not found`});
        }
        user.comparePassword(password, (err, isMatch) => {
            if(err){
                return done(err);
            }
            if(isMatch){
                return done(null, user);
            }
            return done(null, false, {msg: `Invalid email or password`})
        })
    })
}))


