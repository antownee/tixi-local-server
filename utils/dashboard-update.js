'use strict'
//curl -d '{ "auth_token": "YOUR_AUTH_TOKEN", "text": "Hey, Look what I can do!" }' -H "Content-Type: application/json" http://localhost:3030/widgets/welcome
//Widgets:
//-scans
//-merchants
//-portals
//-wallets
//Every minute, update the dasboard by posting to the URL above

const mongoose = require('mongoose');
const async = require('async');
var request = require('request');
//const Model = mongoose.Model;


//Models
var scan = require("../models/scan");
var merchant = require("../models/merchant");
var portal = require("../models/portal");
//REMEBER TO ADD TICKET ORDER COLLECTION
var wallet = require("../models/user");

var widgets = ["scans", "merchants", "portals", "wallets"];
var models = [scan, merchant, portal, wallet];

function getCount(widgetname, model) {
    var url = `http://localhost:3030/widgets/${widgetname}`;
    async.waterfall([
        function (callback) {
            model.count({}, (err, count) => {
                if (err) { callback(err) };

                callback(null, count)
            });
        },
        function (cnt, callback) {
            var resp = JSON.stringify({ current: Math.ceil(Math.random()*10 )});
            request.post({
                url: url,
                body: resp,
                headers: {
                    'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.110 Safari/537.36',
                    'Content-Type': 'application/json'
                },
                method: 'POST'
            }, (err, res, body) => {
                if (!err) {
                    console.log(`Dash updated ${widgetname}`) // Show the HTML for the Google homepage.
                    callback(null);
                }
            })
        }
    ], (err, result) => {
        if (err) { throw new Error(err) };
    })

}

function loopr() {
    for (var index = 0; index < widgets.length; index++) {
        var name = widgets[index];
        var model = models[index];
        getCount(name, model);
    }
}
setInterval(function () {
    loopr();
}, 5*60*1000);