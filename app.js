'use strict';
const express = require('express');
const app = express();
const mongoose = require('mongoose');
const session = require('express-session');
const flash = require('express-flash');
const bodyParser = require('body-parser');
const validator = require('express-validator');
const helmet = require('helmet');
const passport = require('passport');
const MongoStore = require('connect-mongo')(session);
const lusca = require('lusca');

require('dotenv').config();

//Load passport config
require('./utils/passport');

//Db setup
const port = process.env.PORT || 3000;
const mongoUri = process.env.MONGODB_LOCAL || process.env.MONGOLAB_URI;
mongoose.connect(mongoUri);
mongoose.connection.on('connected', function () {
  console.log('MongoDB connection established. ' + mongoUri);
});

mongoose.connection.on('error', function () {
  console.log('MongoDB Connection Error. Please make sure that MongoDB is running.');
  process.exit(1);
});


//Middleware
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static("public")); //Serve up public files
app.use(express.static("node_modules/patternfly/dist"));
app.use(express.static("node_modules"));
app.use(validator());
app.use(session({
  secret: process.env.SESSION_SECRET,
  store: new MongoStore({
    url: mongoUri,
    autoReconnect: true
  }),
  resave: true,
  saveUninitialized: true,
  maxAge: 60000
}));
app.use(flash());
app.use(passport.initialize());
app.use(passport.session());


//SECURITY Middleware
app.use(helmet());
app.disable('x-powered-by');
app.use(lusca({
  csrf: true,
  // csp: { 
  //   policy: {
  //     'default-src': '\'self\'',
  //     'img-src': '*'
  //   }
  // },
  xframe: 'SAMEORIGIN',
  p3p: 'ABCDEF',
  hsts: { maxAge: 31536000, includeSubDomains: true, preload: true },
  xssProtection: true,
  nosniff: true
}));


//View Engine
app.set('view-engine', 'pug');

//Start the Moska broker
require('./controllers/mqtt');

//AUTH
var authRouter = require("./controllers/auth/auth");
app.use(authRouter);
////AUTHENTICATE THE ADMIN ROUTE !
app.use(function (req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  }
  res.redirect('/login');
});

//ADMIN ROUTER
var adminRouter = require("./controllers/admin/admin");
app.use("/admin", adminRouter);

require("./utils/odin.js");

///ERROR HANDLING
// Development
if (process.env.NODE_ENV === 'development') {
  //require('express-debug')(app,{});
  app.use(function (err, req, res, next) {
    console.error(err.stack);
    throw err;
  });
}
// Production
if (process.env.NODE_ENV === 'production') {
  app.use(function (err, req, res, next) {
    console.error(err.stack);
    res.sendStatus(err.status || 500);
  });
}


//Start it up!
app.listen(port, function () {
  console.log(`Tixi local server listening on port ${port}: ${process.env.NODE_ENV} mode`);
});

require("./utils/dashboard-update.js");


